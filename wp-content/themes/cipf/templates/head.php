<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="alternate" type="application/rss+xml" title="<?= get_bloginfo('name'); ?> Feed" href="<?= esc_url(get_feed_link()); ?>">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,300,600,700|Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
    <?php wp_head(); ?>
  </head>
<!------------------------------------------------------------------------------- -->
<!--
						       _                         
						  ___ / | _ __   ___   ___  _ __ 
						 / _ \| || '_ \ / __| / _ \| '__|
						|  __/| || | | |\__ \|  __/| |   
						 \___||_||_| |_||___/ \___||_|   
						                                 
-->
<!------------------------------------------------------------------------------- -->