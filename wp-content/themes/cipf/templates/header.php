<header class="banner navbar navbar-default navbar-static-top" role="banner">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 logo"><a href="<?php bloginfo("url"); ?>"><img src="<?php bloginfo("template_url"); ?>/assets/images/logo.png" alt="<?php bloginfo("name"); ?>" /></a></div><!-- .logo -->
		</div><!-- .row -->
	</div><!-- .container -->
</header>
