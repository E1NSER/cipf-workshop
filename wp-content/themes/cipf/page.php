<!-- ----------------------------------------------------------------------------- -->
<!-- Navigation -->
<!-- ----------------------------------------------------------------------------- -->
<?php use Roots\Sage\Nav; ?>
<div class="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<nav class="collapse navbar-collapse" role="navigation">
		<?php
			if (has_nav_menu('primary_navigation')) :
				wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new Nav\SageNavWalker(), 'menu_class' => 'nav navbar-nav']);
			endif;
		?>
	</nav>
</div><!-- .navigation -->


<?php while (have_posts()) : the_post(); ?>
	<div class="row">
		<div class="main-area <?php if(get_field("big_image")): ?>col-sm-8<?php else: ?>col-sm-12 <?php endif; ?>">
			<?php get_template_part('templates/page', 'header'); ?>
			<?php get_template_part('templates/content', 'page'); ?>
		</div><!-- .main-area -->
		
		<?php if(get_field("big_image")): ?>
			<div class="col-sm-4 images">
				<div class="row">
					<?php  $image = get_field('big_image');
					if( !empty($image) ): ?>
						<div class="col-sm-12 item big">
							<a href="<?php echo $image['url']; ?>" class="fancybox"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
						</div><!-- .col-sm-12 -->
					<?php endif; ?>

					<?php  $image = get_field('left_image');
					if( !empty($image) ): ?>
						<div class="col-sm-6 item">
							<a href="<?php echo $image['url']; ?>" class="fancybox"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
						</div><!-- .col-sm-12 -->
					<?php endif; ?>
					<?php  $image = get_field('right_image');
					if( !empty($image) ): ?>
						<div class="col-sm-6 item">
							<a href="<?php echo $image['url']; ?>" class="fancybox"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
						</div><!-- .col-sm-12 -->
					<?php endif; ?>
					

				</div><!-- .row -->
			</div><!-- .images -->
		<?php endif; ?>
		
	</div><!-- .row -->
<?php endwhile; ?>

<!-- ----------------------------------------------------------------------------- -->
<!-- Meta -->
<!-- ----------------------------------------------------------------------------- -->
<div class="meta clearfix">
	<nav>
		<?php wp_nav_menu(array("menu" => "Footer", "menu_class" => "list-inline")); ?>
	</nav>
</div><!-- .meta -->