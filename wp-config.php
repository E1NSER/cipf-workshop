<?php
/**
 * In dieser Datei werden die Grundeinstellungen für WordPress vorgenommen.
 *
 * Zu diesen Einstellungen gehören: MySQL-Zugangsdaten, Tabellenpräfix,
 * Secret-Keys, Sprache und ABSPATH. Mehr Informationen zur wp-config.php gibt es
 * auf der {@link http://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}
 * Seite im Codex. Die Informationen für die MySQL-Datenbank bekommst du von deinem Webhoster.
 *
 * Diese Datei wird von der wp-config.php-Erzeugungsroutine verwendet. Sie wird ausgeführt,
 * wenn noch keine wp-config.php (aber eine wp-config-sample.php) vorhanden ist,
 * und die Installationsroutine (/wp-admin/install.php) aufgerufen wird.
 * Man kann aber auch direkt in dieser Datei alle Eingaben vornehmen und sie von
 * wp-config-sample.php in wp-config.php umbenennen und die Installation starten.
 *
 * @package WordPress
 */

/**  MySQL Einstellungen - diese Angaben bekommst du von deinem Webhoster. */
/**  Ersetze database_name_here mit dem Namen der Datenbank, die du verwenden möchtest. */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/Applications/MAMP/htdocs/CIPF Workshop/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'db_cipf_workshop');

/** Ersetze username_here mit deinem MySQL-Datenbank-Benutzernamen */
define('DB_USER', 'db');

/** Ersetze password_here mit deinem MySQL-Passwort */
define('DB_PASSWORD', 'bichewgy');

/** Ersetze localhost mit der MySQL-Serveradresse */
define('DB_HOST', 'localhost');

/** Der Datenbankzeichensatz der beim Erstellen der Datenbanktabellen verwendet werden soll */
define('DB_CHARSET', 'utf8');

/** Der collate type sollte nicht geändert werden */
define('DB_COLLATE', '');

/**#@+
 * Sicherheitsschlüssel
 *
 * Ändere jeden KEY in eine beliebige, möglichst einzigartige Phrase.
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * kannst du dir alle KEYS generieren lassen.
 * Bitte trage für jeden KEY eine eigene Phrase ein. Du kannst die Schlüssel jederzeit wieder ändern,
 * alle angemeldeten Benutzer müssen sich danach erneut anmelden.
 *
 * @seit 2.6.0
 */
define('AUTH_KEY',         '/DW$jH:sB:WriRH<-L[C[cE*4>O:!xRAY_!q[a:^UMAiVuV@+w+f1~E][TXa77V>');
define('SECURE_AUTH_KEY',  'c,O&gPr;)]Z?~g-IrpnhpaCd42,cKx9)0eBv$Fw,b3<18ij|]gq2+XZ|3lZ0;/ x');
define('LOGGED_IN_KEY',    'QbH_z0jFlc[ZdQN=^67BOp*yCqgqnE_r}s,~!4R&bP{0je#Ap&J;o%|/sO@~-G?@');
define('NONCE_KEY',        '>RRaJLJ7_?p##N(D?kR|b7|yZ0#DW^Q_-ICpeF`+cj;mq1)+mt,~0K8(aHmjbV5?');
define('AUTH_SALT',        'p,c[aIt]uDZ)<pA&D?m5Y(;n+S8p#9=-7eKQgU!_h&xJDWn&L}^mbpmF;MBr&h(>');
define('SECURE_AUTH_SALT', '7A[tUD|t:$U)JoecEnD-B3];-vD<g?^Wt<4zISX-6?XO8|!?Y_U7{w:2S{{3|4U2');
define('LOGGED_IN_SALT',   'cm+jRf|c%QZW|ZGCCT:deu+Mo5u-[xBrYvbhUpK!e/[Ef[BLyk*?O5`MC;yGycQy');
define('NONCE_SALT',       ']%7e@W%etO)j(;}/|:hi31+wT*Hq:}]Nk;+{)Xtx-ye#7k2m;&iMv0Fw4(n%zi~@');

/**#@-*/

/**
 * WordPress Datenbanktabellen-Präfix
 *
 *  Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank
 *  verschiedene WordPress-Installationen betreiben. Nur Zahlen, Buchstaben und Unterstriche bitte!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
//define('WP_ENV', 'development');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
